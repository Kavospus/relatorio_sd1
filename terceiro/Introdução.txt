1.	INTRODU��O
Atualmente a sociedade � quase que inteiramente dependente de c�lculos num�ricos, esses c�lculos ganham uma complexibilidade que torna invi�vel a sua soma manualmente, ent�o utiliza-se de calculadoras, microcomputadores e outros aparelhos eletr�nicos similares.
Os circuitos realizadores de tais opera��es s�o denominados, de circuitos aritm�ticos. Tais circuitos utilizam-se de opera��es com portas logicas, e sua complexibilidade depende do qu�o complexo � o sistema trabalhado, com a utiliza��o de uma grande quantidade de CI�s.
Este Relat�rio baseia-se na implementa��o dos circuitos Somadores e Subtratores, os quais utilizam-se de opera��es booleanas, para a realiza��o de soma e subtra��o.
	Este Relat�rio foi dividido para uma melhor organiza��o e entendimento acerca dos assuntos tratados, no experimento. Primeiramente introduzem-se os objetivos referentes � pratica experimental, subsequente h� a partes experimental, com as informa��es referentes a montagem do experimento.
	Depois tem-se a apresenta��o dos resultados obtidos, com discuss�o sobre a metodologia e resultados encontrados, com uma compara��o com os resultados esperados e por fim, faz-se uma conclus�o das sobre todo o processo envolvendo os erros e �xitos adquiridos.


