1.1	Introdu��o

O experimento teve como proposito familiarizar os alunos com alguns dos principais tipos de CI's, os circuitos integrados CMOS( complementary Metal-Oxide-Semiconductor) e TTL (Transsistor Logic). 
Tais CI's devem ser estudados por serem bastantes difundidos no mercado de semicondutores. 
Ent�o este relat�rio ir� oferecer um estudo das propriedades b�sicas dos mesmos, como a curva de transfer�ncia de tens�o, par�metros el�tricos e est�ticos, al�m dos par�metros de tempo, de cada um dos dois circuitos.

